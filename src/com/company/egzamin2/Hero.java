package com.company.egzamin2;

import java.util.Random;

public abstract class Hero {

    static String[] names = new String[]{
            "Merlin",
            "Artur",
            "Villain",
            "Bulldozer",
            "Vytaus"
    };

    protected String name;

    protected int health;
    protected int strength;

    public Hero(String name, int health, int strength) {
        this.name = name;
        this.health = health;
        this.strength = strength;
    }

    public abstract void attack(Hero hero);

    public static Hero generateRandomHero() {
        double rand = Math.random();
        Random random = new Random();
        String name = names[random.nextInt(names.length)];
        int hp = random.nextInt(300) + 100;
        Hero hero;
        if (rand > 0.5) {
            hero = new Wizard(name, hp, random.nextInt(20), random.nextInt(50));
        } else {
            hero = new Knight(name, hp, random.nextInt(60));
        }
        return hero;
    }
}
