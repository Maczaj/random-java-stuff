package com.company.egzamin2;

import java.util.Random;

public class Wizard extends Hero {

    private int mana;

    public Wizard(String name, int health, int strength, int mana) {
        super(name, health, strength);
        this.mana = mana;
    }


    @Override
    public String toString() {
        return "Wizard{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", strength=" + strength +
                ", mana=" + mana +
                '}';
    }

    @Override
    public void attack(Hero hero) {
        hero.health = hero.health - new Random().nextInt(strength / 2);
    }

    public void magicAttack(Hero hero) {
        hero.health = hero.health - new Random().nextInt(mana);
    }
}
