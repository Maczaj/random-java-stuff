package com.company.egzamin2;

import java.util.Random;

public class Knight extends Hero {

    public Knight(String name, int health, int strength) {
        super(name, health, strength);
    }

    @Override
    public void attack(Hero hero) {
        hero.health = hero.health - new Random().nextInt(strength);
    }

    @Override
    public String toString() {
        return "Knight{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", strength=" + strength +
                '}';
    }
}
