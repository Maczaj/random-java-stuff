package com.company.q1;

public abstract class Figure {

    public abstract double getPerimeter();

    public abstract double getField();

}
