package com.company.q1;

public class Circle extends Figure {

    private final double r;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * r;
    }

    @Override
    public double getField() {
        return Math.PI * Math.pow(r, 2.0);
    }
}
