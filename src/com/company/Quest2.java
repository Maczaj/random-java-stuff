package com.company;

import java.util.*;

public class Quest2 {

    public static void main(String[] args) {
        List<Integer> ar1 = new ArrayList<>();
        List<Integer> ar2 = new ArrayList<>();
        Random random = new Random();

        for (int counter = 0; counter <= 50; ++counter) {
            ar1.add(random.nextInt(31) - 10);
            ar2.add(random.nextInt(41) + 10);
        }

        System.out.println(ar1);
        System.out.println(ar2);

//        nie rozumiem o co chodzi pod pojeciem metod frameworku Collections
        List<Integer> ar3 = new ArrayList<>();
//        moze byc java 8 czy lepiej nie ?
        ar1.stream().filter(num -> !ar2.contains(num)).forEach(ar3::add);
        ar2.stream().filter(num -> !ar1.contains(num)).forEach(ar3::add);

        System.out.println("ar3 is " + ar3);

        System.out.println(Collections.frequency(ar3, 7));

        Set<Integer> asSet = new HashSet<>(ar3);
        List<Integer> listWithoutDuplicates = new ArrayList<>(asSet);
        listWithoutDuplicates.sort(Comparator.reverseOrder());
        System.out.println(listWithoutDuplicates);

    }
}
