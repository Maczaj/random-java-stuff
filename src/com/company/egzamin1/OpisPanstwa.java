package com.company.egzamin1;

public class OpisPanstwa {
    String stolica;
    String kontynent;

    @Override
    public String toString() {
        return "OpisPanstwa{" +
                "stolica='" + stolica + '\'' +
                ", kontynent='" + kontynent + '\'' +
                '}';
    }

    public OpisPanstwa(String stolica, String kontynent) {
        this.stolica = stolica;
        this.kontynent = kontynent;
    }

    public String getStolica() {
        return stolica;
    }

    public String getKontynent() {
        return kontynent;
    }
}
