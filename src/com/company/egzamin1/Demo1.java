package com.company.egzamin1;

public class Demo1 {

    public static void main(String[] args) {
        AtlasPanstw atlas = new AtlasPanstw();
        atlas.put("Japan", new OpisPanstwa("Tokyo", "Asia"));
        atlas.put("Poland", new OpisPanstwa("Warsaw", "Europe"));
        atlas.put("Italy", new OpisPanstwa("Rome", "Europe"));
        atlas.put("Columbia", new OpisPanstwa("Bogota", "South America"));

        System.out.println("Info na temat Rzymu" + atlas.znajdzPanstwoPoStolicy("Rome"));
        System.out.println("Lwowa nie ma w bazie wiec null: " + atlas.znajdzPanstwoPoStolicy("Lviv"));

        System.out.println("Kraje z Europy " + atlas.znajdzZbiorPanstwZKontynentu("Europe"));
    }
}
