package com.company.egzamin1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.Collectors;

public class AtlasPanstw extends HashMap<String, OpisPanstwa> {

    public OpisPanstwa znajdzPanstwoPoStolicy(String capitalName) {

        return this.entrySet().stream()
                .filter(entry -> entry.getValue().getStolica().equals(capitalName))
                .findAny()
                .map(entry -> entry.getValue())
                .orElse(null);
    }

    public HashSet<OpisPanstwa> znajdzZbiorPanstwZKontynentu(String kontynent) {
        return this.values()
                .stream()
                .filter(state -> state.getKontynent().equals(kontynent))
                .collect(Collectors.toCollection(HashSet::new));
    }
}
