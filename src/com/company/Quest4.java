package com.company;

import com.company.q4.Country;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Quest4 {

    public static void main(String[] args) {
        List<Country> alliance = new ArrayList<>();
        alliance.add(new Country("Poland", "Warsaw"));
        alliance.add(new Country("Czech Republic", "Prague"));
        alliance.add(new Country("Hungary", "Budapest"));
        System.out.println(alliance);
        Collections.sort(alliance);
        System.out.println(alliance);
    }
}
