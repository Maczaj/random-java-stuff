package com.company;

import com.company.q3.MyException;

import java.util.Scanner;

public class Quest3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        if (input < 0) {
            try {
                throw new MyException();
            } catch (MyException e) {
                e.printStackTrace();
                System.err.println("Negative value provided!");
            }
        }
    }
}
