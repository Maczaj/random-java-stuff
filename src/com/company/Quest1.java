package com.company;

import com.company.q1.Circle;
import com.company.q1.Rectangle;

public class Quest1 {

    public static void main(String[] args) {
        System.out.println("I want rectangle with a=2.0 and b=5.0");
        Rectangle rectangle = new Rectangle(2.0, 5.0);
        System.out.println("Field = " + rectangle.getField());
        System.out.println("Perimeter = " + rectangle.getPerimeter());

        System.out.println("I want circle with radius=5.0");
        Circle circle = new Circle(5.0);
        System.out.println("Field = " + circle.getField());
        System.out.println("Perimeter = " + circle.getPerimeter());
    }
}
